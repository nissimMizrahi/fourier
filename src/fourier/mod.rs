pub use std::f64;

#[derive(Debug, PartialEq, Clone)]
pub struct Signal
{
    pub real: f64,
    pub i: f64,
    pub freq: i64
}

impl Signal
{
    pub fn kill(&mut self)
    {
        self.real = 0.0;
        self.i = 0.0;
    }

    pub fn mul(&mut self, x: f64)
    {
        self.real *= x;
        self.i *= x;
    }
    pub fn mul_signal(&mut self, other: &Signal)
    {
        let t_real = self.real * other.real - self.i * other.i;
        let t_im = self.real * other.i + self.i * other.real;

        self.i = t_im;
        self.real = t_real;
    }

    pub fn add(&mut self, other: &Signal)
    {
        self.real += other.real;
        self.i += other.i;
    }

    pub fn amp(&self) -> f64
    {
        return (self.real.powi(2) + self.i.powi(2)).sqrt();
    }
    pub fn phase(&self) -> f64
    {
        return (self.i / (if self.real != 0.0 {self.real} else {1.0})).atan();
    }
}

pub fn dft(signal: &Vec<f64>) -> Vec<Signal>
{
    let pi = f64::consts::PI;
    let N = signal.len();

    let mut ret: Vec<Signal> = vec![Signal{real: 0.0, i: 0.0, freq: 0}; N];

    for k in {0..N}
    {
        for n in {0..N}
        {
            let num = (2.0 * pi * k as f64 * n as f64) / N as f64;

            let mut part = Signal
            {
                real: num.cos(),
                i: (-1.0) * num.sin(),
                freq: 0
            };
            part.mul(signal[n]);

            ret[k].add(&part);  
        }
        ret[k].mul(1.0 / N as f64);
        ret[k].freq = k as i64;
    }

    return ret;
}

pub fn compile_signals(signals: &Vec<Signal>) -> Vec<f64>
{
    let pi = f64::consts::PI;
    let N = signals.len();
    let  dt = pi * 2.0 / N as f64;
    let mut time = 0.0;

    let mut ret: Vec<f64> = vec![0.0; N];

    for k in {0..N}
    {
        for i in {0..N}
        {
            let s = &signals[i];
            ret[k] += s.amp() * ((s.freq as f64 * time) + s.phase() + (pi / 2.0)).sin();
        }
        time += dt;
    }

    return ret;

}

pub fn idft(signals: &Vec<Signal>) -> Vec<f64>
{
    let pi = f64::consts::PI;
    let N = signals.len();

    let mut ret: Vec<f64> = vec![0.0; N];

    for n in {0..N}
    {
        let mut sum = Signal{
            real: 0.0,
            i: 0.0,
            freq: n as i64
        };
        for k in {0..N}
        {
            let num = (2.0 * pi * k as f64 * n as f64) / N as f64;

            let mut tmp = signals[k].clone();
            tmp.mul_signal(&Signal{
                real: num.cos(),
                i: num.sin(),
                freq: 0
            });
            sum.add(&tmp);
            
        }
        ret[n] = sum.real;
    }

    return ret;

}