extern crate minimp3;
use minimp3::{Decoder, Frame, Error};

use std::fs::File;

mod fourier;

fn main() 
{
    /*
    let signal = vec![
        100.0, 100.0, 100.0,
        -100.0, -100.0, -100.0,
        100.0, 100.0, 100.0,
        -100.0, -100.0, -100.0
    ];

    let mut f = fourier::dft(&signal);
    */

    let mut decoder = Decoder::new(File::open("Boys Are Back In Town Thin Lizzy.mp3").unwrap());

    while let Ok(ref mut Frame) = decoder.next_frame() 
    {       
        println!("Decoded {} samples {}", (144 * Frame.bitrate  * 1000) / Frame.sample_rate, Frame.data.len());
    }


}
